
# Installation des tools nécessaire a Inkcut : 

> Actuellement Inkcut de ne fournit pas encore de package d'installation sous Windows.
> 
> L'une des difficultés pour les utilisateurs et d'avoir des instructions détaillé sur la mise en place et la configuration de Inkcut, alors voici un tutoral "Step By Step" pour vous gider.

# I - Installation de python :

Tout d'abord, allez télécharger le package d'installation sur cette url : 
*(choisir le package adapté au système du système utilisé comme Windows, Linux, Mac OS, ...)* 
```
https://www.python.org/downloads/
```
Puis, exécutez le fichier exécutable du programme d'installation Python que vous avez téléchargé.

Sélectionnez les options suivantes dans la fenêtre du programme d'installation de Python pour configurer celui-ci de la manière optimale : 

- Dans la première page de l'installateur, séléctionner "Customier installation", puis cocher l'option "Add python.exe to PATH".

- Dans la deuxième page, cocher l'ensemble des options de "Optional Features".

- Dans la dernière page, cocher l'ensemble des options de "Advanced Options".

### __Attention__


Plusieurs problèmes peuvent être rencontrés lors de son installation :

- Le premier, un problème de compatibilité de votre Python qui peut être dû à une version de celui-ci obsolète. Vérification de la version : Python version

- Le deuxième, que vous ne possédez pas les privilèges administrateur, c’est pourquoi je vous invite à ne pas sélectionner les options qui requièrent ces privilèges.

Une fois l’installation terminée, vérifier que celui-ci est bien installé et avec la bonne version avec la commande :
```
python --version 
```

# II - Installation des outils de génération de Visual Studio :

Télécharger l’exécutable via ce lien :
```
https://download.visualstudio.microsoft.com/download/pr/1e5ff7fe-162b-4a3d-8fda-3267702b551d/e25ce34fd81235ebbd79010afad8e63f/vs_buildtools.exe
```

Procédure :

Une fois l’exécutable lancé, débuter l’installation.

Une fois la page d’installation Charge de travail ouverte, continuer à installer sans sélectionner d’autres options.

Puis une fois celui-ci installé, lancer Visual Studio Buil Tools 2019 :

# III - Installation des packets Inkcut :

- Ouvre votre invite de commande (cmd), puis débuter l'installation en entrant la commande "pip3 install PyQt5 inkcut".

- Une fois celle-ci terminé, toujours dans l'invide de commande, lancer l'application en tapant "inkcut".

__Fin de l'installation__
